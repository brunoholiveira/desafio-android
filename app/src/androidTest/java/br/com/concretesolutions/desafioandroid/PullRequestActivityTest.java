package br.com.concretesolutions.desafioandroid;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.espresso.intent.Intents;
import android.support.test.espresso.intent.VerificationMode;
import android.support.test.espresso.intent.matcher.IntentMatchers;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.concretesolutions.desafioandroid.activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.intent.Intents.init;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.Intents.release;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static br.com.concretesolutions.desafioandroid.CustomMatchers.withSize;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class PullRequestActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void initCorrectly() {
        onView(withId(R.id.list_repositories)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.opened_text)).check(matches(isDisplayed()));
        onView(withId(R.id.opened_text)).check(matches(withText("30 opened")));
        onView(withId(R.id.closed_text)).check(matches(isDisplayed()));
        onView(withId(R.id.closed_text)).check(matches(withText(" / 0 closed")));
        onView(withId(R.id.pull_request_list)).check(matches(withSize(30)));
    }

    @Test
    public void clickRecyclerViewItem() {
        onView(withId(R.id.list_repositories)).perform(actionOnItemAtPosition(0, click()));
        init();
        intending(allOf(hasAction(Intent.ACTION_VIEW), hasData("https://github.com/facebook/react-native/pull/10526")))
            .respondWith(new Instrumentation.ActivityResult(0, null));
        onView(withId(R.id.pull_request_list)).perform(actionOnItemAtPosition(0, click()));
        intended(allOf(hasAction(Intent.ACTION_VIEW), hasData("https://github.com/facebook/react-native/pull/10526")));
        release();
    }

}