package br.com.concretesolutions.desafioandroid;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class CustomMatchers {

    public static Matcher<View> withSize(final int size) {
        return new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                return ((RecyclerView) item).getAdapter().getItemCount() == size;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("RecyclerView should have " + size + " items");
            }
        };
    }

}
