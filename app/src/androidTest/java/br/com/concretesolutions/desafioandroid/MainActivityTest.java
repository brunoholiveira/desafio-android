package br.com.concretesolutions.desafioandroid;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import br.com.concretesolutions.desafioandroid.activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static br.com.concretesolutions.desafioandroid.CustomMatchers.withSize;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void initCorrectly() {
        onView(withId(R.id.list_repositories)).check(matches(withSize(30)));
    }

    @Test
    public void clickRecyclerViewItem() {
        onView(withId(R.id.list_repositories)).perform(actionOnItemAtPosition(0, click()));
        onView(withId(R.id.opened_text)).check(matches(isDisplayed()));
    }



}
