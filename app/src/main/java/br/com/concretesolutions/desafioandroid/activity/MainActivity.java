package br.com.concretesolutions.desafioandroid.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import br.com.concretesolutions.desafioandroid.R;
import br.com.concretesolutions.desafioandroid.adapter.RepositoryAdapter;
import br.com.concretesolutions.desafioandroid.listener.InfiniteScrollListener;
import br.com.concretesolutions.desafioandroid.listener.RecyclerViewTouchListener;
import br.com.concretesolutions.desafioandroid.model.Repositories;
import br.com.concretesolutions.desafioandroid.model.Repository;
import br.com.concretesolutions.desafioandroid.service.GithubService;
import br.com.concretesolutions.desafioandroid.ui.DividerItemDecoration;

public class MainActivity extends AppCompatActivity implements RecyclerViewTouchListener.ClickListener {

    private RecyclerView listRepositories;

    private GithubService githubService;

    private List<Repository> repositories;

    public static final String URL_EXTRA = "url";
    public static final String REPO_NAME_EXTRA = "repo_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.listRepositories = (RecyclerView) findViewById(R.id.list_repositories);
        this.githubService = new GithubService();
        repositories = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listRepositories.setLayoutManager(layoutManager);
        listRepositories.setItemAnimator(new DefaultItemAnimator());
        listRepositories.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        listRepositories.setAdapter(new RepositoryAdapter(repositories));
        this.listRepositories.addOnScrollListener(new InfiniteScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.d("Github", "Looking for repositories from page " + page);
                new RepositoryTask().execute(page);
            }
        });
        this.listRepositories.addOnItemTouchListener(new RecyclerViewTouchListener(this, this));
        new RepositoryTask().execute(1);
    }

    @Override
    public void onClick(View view, int position) {
        Repository repository = repositories.get(position);
        Intent intent = new Intent(MainActivity.this, PullRequestActivity.class);
        intent.putExtra(URL_EXTRA, repository.getPullUrl().replace("{/number}", ""));
        intent.putExtra(REPO_NAME_EXTRA, repository.getName());
        startActivity(intent);
    }

    private class RepositoryTask extends AsyncTask<Integer, Void, Repositories> {

        @Override
        protected Repositories doInBackground(Integer... integers) {
            int page = integers[0];
            return githubService.getRepositories(page);
        }

        @Override
        protected void onPostExecute(Repositories repositories) {
            if (repositories != null) {
                MainActivity.this.repositories.addAll(repositories.getItems());
                listRepositories.getAdapter().notifyDataSetChanged();
            }
        }
    }
}
