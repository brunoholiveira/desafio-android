package br.com.concretesolutions.desafioandroid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.concretesolutions.desafioandroid.R;
import br.com.concretesolutions.desafioandroid.adapter.PullRequestAdapter;
import br.com.concretesolutions.desafioandroid.listener.RecyclerViewTouchListener;
import br.com.concretesolutions.desafioandroid.model.PullRequest;
import br.com.concretesolutions.desafioandroid.service.GithubService;
import br.com.concretesolutions.desafioandroid.ui.DividerItemDecoration;

public class PullRequestActivity extends AppCompatActivity implements RecyclerViewTouchListener.ClickListener {

    private RecyclerView listPullRequests;
    private TextView openText;
    private TextView closedText;

    private List<PullRequest> pullRequests;

    private GithubService githubService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listPullRequests = (RecyclerView) findViewById(R.id.pull_request_list);
        openText = (TextView) findViewById(R.id.opened_text);
        closedText = (TextView) findViewById(R.id.closed_text);
        String url = getIntent().getStringExtra(MainActivity.URL_EXTRA);
        String repoName = getIntent().getStringExtra(MainActivity.REPO_NAME_EXTRA);
        setTitle(repoName);
        githubService = new GithubService();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listPullRequests.setLayoutManager(layoutManager);
        listPullRequests.setItemAnimator(new DefaultItemAnimator());
        listPullRequests.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        listPullRequests.addOnItemTouchListener(new RecyclerViewTouchListener(this, this));
        new PullRequestTask().execute(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(pullRequest.getUrl()));
        startActivity(intent);
    }

    private class PullRequestTask extends AsyncTask<String, Void, List<PullRequest>> {

        @Override
        protected List<PullRequest> doInBackground(String... strings) {
            String url = strings[0];
            return githubService.getPullRequests(url);
        }

        @Override
        protected void onPostExecute(List<PullRequest> pullRequests) {
            PullRequestActivity.this.listPullRequests.setAdapter(new PullRequestAdapter(pullRequests));
            PullRequestActivity.this.pullRequests = pullRequests;
            if (pullRequests != null) {
                int open = 0, closed = 0;
                for (PullRequest pullRequest : pullRequests) {
                    if (pullRequest.isOpen()) {
                        open++;
                    } else {
                        closed++;
                    }
                }
                PullRequestActivity.this.openText.setText(String.format("%d opened", open));
                PullRequestActivity.this.closedText.setText(String.format(" / %d closed", closed));
            }
        }
    }

}
