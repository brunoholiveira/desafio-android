package br.com.concretesolutions.desafioandroid.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concretesolutions.desafioandroid.R;
import br.com.concretesolutions.desafioandroid.model.Repository;

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.RepositoryViewHolder> {

    private List<Repository> repositories;

    public RepositoryAdapter(List<Repository> objects) {
        this.repositories = objects;
    }

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.repo_list_item, viewGroup, false);
        return new RepositoryViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder viewHolder, int position) {
        Repository repo = repositories.get(position);
        viewHolder.repoName.setText(repo.getName());
        viewHolder.repoDesc.setText(repo.getDescription());
        viewHolder.forks.setText(String.valueOf(repo.getForks()));
        viewHolder.stars.setText(String.valueOf(repo.getStars()));
        new Picasso.Builder(viewHolder.itemView.getContext())
                .downloader(new OkHttpDownloader(viewHolder.itemView.getContext(), Integer.MAX_VALUE))
                .build()
                .load(repo.getOwner().getAvatar())
                .resize(64, 64)
                .into(viewHolder.avatar);
        viewHolder.username.setText(String.valueOf(repo.getOwner().getUsername()));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class RepositoryViewHolder extends RecyclerView.ViewHolder {

        TextView repoName;
        TextView repoDesc;
        TextView forks;
        TextView stars;
        ImageView avatar;
        TextView username;

        public RepositoryViewHolder(View itemView) {
            super(itemView);
            repoName = (TextView) itemView.findViewById(R.id.repo_name_text);
            repoDesc = (TextView) itemView.findViewById(R.id.repo_desc_text);
            forks = (TextView) itemView.findViewById(R.id.forks_text);
            stars = (TextView) itemView.findViewById(R.id.stars_text);
            avatar = (ImageView) itemView.findViewById(R.id.profile_image);
            username = (TextView) itemView.findViewById(R.id.username_text);
        }

    }

}