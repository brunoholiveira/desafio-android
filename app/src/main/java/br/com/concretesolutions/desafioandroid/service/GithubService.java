package br.com.concretesolutions.desafioandroid.service;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import br.com.concretesolutions.desafioandroid.model.PullRequest;
import br.com.concretesolutions.desafioandroid.model.Repositories;

public class GithubService {

    private static final String REPO_URL = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";

    public Repositories getRepositories(int page) {
        try {
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            return template.getForObject(REPO_URL + page, Repositories.class);
        } catch (HttpClientErrorException e) {
            return null;
        }
    }

    public List<PullRequest> getPullRequests(String url) {
        try {
            RestTemplate template = new RestTemplate();
            template.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            ResponseEntity<List<PullRequest>> response = template.exchange(url, HttpMethod.GET, null,
                    new ParameterizedTypeReference<List<PullRequest>>() {});
            return response.getBody();
        } catch (HttpClientErrorException e) {
            return null;
        }
    }

}