package br.com.concretesolutions.desafioandroid.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concretesolutions.desafioandroid.R;
import br.com.concretesolutions.desafioandroid.model.PullRequest;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.PullRequestViewHolder> {

    private List<PullRequest> pullRequests;

    public PullRequestAdapter(List<PullRequest> objects) {
        this.pullRequests = objects;
    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.pull_request_list_item, viewGroup, false);
        return new PullRequestViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PullRequestViewHolder viewHolder, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        viewHolder.pullName.setText(pullRequest.getTitle());
        viewHolder.pullDesc.setText(pullRequest.getBody());
        new Picasso.Builder(viewHolder.itemView.getContext())
                .downloader(new OkHttpDownloader(viewHolder.itemView.getContext(), Integer.MAX_VALUE))
                .build()
                .load(pullRequest.getUser().getAvatar())
                .resize(48, 48)
                .into(viewHolder.avatar);
        viewHolder.username.setText(String.valueOf(pullRequest.getUser().getLogin()));
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class PullRequestViewHolder extends RecyclerView.ViewHolder {

        TextView pullName;
        TextView pullDesc;
        ImageView avatar;
        TextView username;

        public PullRequestViewHolder(View itemView) {
            super(itemView);
            pullName = (TextView) itemView.findViewById(R.id.pull_name_text);
            pullDesc = (TextView) itemView.findViewById(R.id.pull_desc_text);
            avatar = (ImageView) itemView.findViewById(R.id.profile_image);
            username = (TextView) itemView.findViewById(R.id.username_text);
        }

    }

}